<?php

namespace app\modules\admin;
use yii\filters\AccessControl;
/**
 * admin module definition class
 */
class AdminModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';
//    public $defaultRoute = 'default'; // where home refers to HomeController under modules/currency/controllers
    /**
     * @inheritdoc
     */
    public function init()
    {
      $this->layout = '@app/modules/admin/views/layouts/main';
        parent::init();

        // custom initialization code goes here
    }
    
    
    
    /**
     * @inheritdoc
     */
//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
////                        'modules' => 'admin',
//                        'controllers' => 'admin/default',
//                        'actions' => ['login', 'error'],
//                        'allow' => true,
//                    ],   
//                    [
//                        'actions' => ['index','statistics', 'logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            
//        ];
//    }
    
    
//    public function beforeAction($action)
//{
//    if (!parent::beforeAction($action)) {
//        return false;
//    }
//
//    if (!\Yii::$app->user->can('admin')) {
////        throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page.');
//    
//        return $this->render('login');
//    }
//
//    return true;
//}
    
}
