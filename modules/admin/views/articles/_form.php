<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categorys;
use vova07\imperavi\Widget as ImperaviWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Articles */
/* @var $form yii\widgets\ActiveForm */

$categorys = Categorys::find()->all();
$categorysList = array();

if ($categorys) {
  foreach ($categorys as $singleCategory) {
    $categorysList[$singleCategory->id] = $singleCategory->title;
  }
}
?>




<div class="articles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
            $form->field($model, 'category_id')
//            ->textInput()
            ->dropDownList($categorysList);
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?=
    $form->field($model, 'content')->widget(ImperaviWidget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen'
            ]
]])
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
