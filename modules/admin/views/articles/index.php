<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ArticlesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="articles-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <p>
        <?= Html::a(Yii::t('admin', 'Create Articles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            [ 'attribute' => 'id',
                'headerOptions' => ['width' => '70'],
                'contentOptions' => ['style' => 'max-width: 30px;overflow: auto;']
            ],
            [ 'attribute' => 'category_id',
//                'label' => 'Короткое описание',
                'headerOptions' => ['width' => '200'],
                'contentOptions' => ['style' => 'max-width: 200px;overflow: auto;']
            ],
            [ 'attribute' => 'title',
//                'label' => 'Короткое описание',
                'headerOptions' => ['width' => '300'],
                'contentOptions' => ['style' => 'max-width: 300px;overflow: auto;']
            ],
            [ 'attribute' => 'description',
//                'label' => 'Короткое описание',
                'headerOptions' => ['width' => '200'],
                'contentOptions' => ['style' => 'max-width: 200px;overflow: auto;']
            ],
            [ 'attribute' => 'created_at',
//                'label' => 'Короткое описание',
                'headerOptions' => ['width' => '200'],
                'contentOptions' => ['style' => 'max-width: 200px;overflow: auto;']
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?php Pjax::end(); ?></div>
