<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller {
  /**
   * @inheritdoc
   */
//    public function behaviors() {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'rules' => [
//                    [
////                        'actions' => ['index','login'],
//                        'actions' => ['login'],
//                        'allow' => true,
//                    ],
//                    [
//                        'actions' => ['logout','statistics'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }

  /**
   * Renders the index view for the module
   * @return string
   */
  public function actionIndex() {

//    if (Yii::$app->user->isGuest) {
//      $this->layout = '@app/modules/admin/views/layouts/login';
//      $model = new LoginForm();
//      if ($model->load(Yii::$app->request->post()) && $model->login()) {
////                return $this->goBack();
//        return $this->redirect(['dashboard/']);
//      }
//
//      return $this->render('login', [
//                  'model' => $model,
//      ]);
//    }
    return $this->redirect(['dashboard/']);
  }
//  public function actionLogin() {
//
//    if (!Yii::$app->user->isGuest) {
//      return $this->redirect(['default/index']);
//    }
//    $this->layout = '@app/modules/admin/views/layouts/login';
//
//    $model = new LoginForm();
//    if ($model->load(Yii::$app->request->post()) && $model->login()) {
////                return $this->goBack();
//      return $this->redirect(['default/index']);
//    }
//
//    return $this->render('login', [
//                'model' => $model,
//    ]);
//  }

  /**
   * Logout action.
   *
   * @return string
   */
  public function actionLogout() {
    Yii::$app->user->logout();

    return $this->redirect(['default/login']);
  }

  /**
   * Logout action.
   *
   * @return string
   */
  public function actionStatistics() {
    return $this->render('statistics');
  }

}
