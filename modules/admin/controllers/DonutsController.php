<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Donuts;
use app\models\DonutsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DonutsController implements the CRUD actions for Donuts model.
 */
class DonutsController extends Controller
{

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'delete' => ['POST'],
            ],
        ],
    ];
  }

  /**
   * Lists all Donuts models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new DonutsSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Donuts model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id)
  {
    return $this->render('view', [
                'model' => $this->findModel($id),
    ]);
  }

  /**
   * Creates a new Donuts model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate()
  {
    $model = new Donuts();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      // process uploaded image file instance
      $image = $model->uploadImage();
      $image2 = $model->uploadImage(1);
      $image3 = $model->uploadImage(2);
     
      
      if ($model->save()) {
        
        if ($image !== false) {
          $path = $model->getImageFile();
//           var_dump($path);die();
          $image->saveAs($path);
        }
        
        if ($image2 !== false) {
          $path2 = $model->getImageFile(1);
          $image2->saveAs($path2);
        }
        if ($image3 !== false) {
          $path3 = $model->getImageFile(2);
          $image3->saveAs($path3);
        }
        
//        var_dump($model);die();
        
      } else {
        // error in saving model
      }
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
                  'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Donuts model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id)
  {
    $model = $this->findModel($id);
//    $oldFile = $model->getImageFilePath();
//    $oldFile2 = $model->getImageFilePath(1);
//    $oldFile3 = $model->getImageFilePath(2);
//    $oldAvatar = $model->img;
////    $oldFileName = $model->filename;

//    if ($model->load(Yii::$app->request->post())) {
//      // process uploaded image file instance
//      $image = $model->uploadImage();
////            echo '<pre>';
////            var_dump($image);die();
//      // revert back if no valid file instance uploaded
//      if ($image === false) {
//        $model->img = $oldAvatar;
//        $model->filename = $oldFileName;
//      }
//
//      if ($model->save()) {
//        // upload only if valid uploaded file instance found
//        if ($image !== false) { // delete old and overwrite
//          if (file_exists($oldFile)){
//            
////            var_dump($oldFile);
//            unlink($oldFile);
////          die();
//            
//          }
//
//          $path = $model->getImageFile();
//          $image->saveAs($path);
//        }
//        return $this->redirect(['view', 'id' => $model->id]);
//      } else {
//        // error in saving model
//      }
//    }
    return $this->render('update', [
                'model' => $model,
    ]);
  }

  /**
   * Deletes an existing Donuts model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id)
  {
    $model = $this->findModel($id);
    
//    var_dump($model->getImageFilePath());die;
    // validate deletion and on failure process any exception 
    // e.g. display an error message 
    if ($model->delete()) {
      if (!$model->deleteImage()) {
        Yii::$app->session->setFlash('error', 'Error deleting image');
      }
    }
    return $this->redirect(['index']);
  }

  /**
   * Finds the Donuts model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Donuts the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = Donuts::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

}
