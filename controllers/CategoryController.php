<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\swiftmailer\Mailer;
use app\components\behaviors\EmailBehavior;
use app\models\User;
use app\models\SignupForm;
use yii\data\Pagination;

//use yii\base\Controller;
class CategoryController extends Controller
{

  public $layout = 'digishop';

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
// ...
//        'access' => [
//            'class' => AccessControl::className(),
//            'only' => ['logout'],
//            'rules' => [
//                [
//                    'actions' => ['logout'],
//                    'allow' => true,
//                    'roles' => ['@'],
//                ],
//                [
//                    'actions' => ['login'],
//                    'allow' => true,
//                    'roles' => ['?'],
//                ],
//            ],
//        ],
//        'EmailBehavior' => [
//            'class' => EmailBehavior::className(),
//            'attributes' => [
//                Controller::EVENT_AFTER_ACTION => 'sendEmail',
//            ]
//        ],
            // ...
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex($slug = null)
  {

    if (empty($slug) || $slug == 'index') {

      // выводит список всех категорий СДЕЛАТЬ


      return $this->redirect(Yii::$app->request->referrer);
    } else {
//      var_dump($slug);
//      die();


      $model = \app\models\Categorys::find()->where(['slug' => $slug])->one();

      if (!is_null($model)) {

//        $articles = \app\models\Articles::find()->where(['category_id' => $model->id])->all();
        $query = \app\models\Articles::find()->andWhere(['category_id' => $model->id])->orderBy('created_at DESC');
//        $countQuery = clone $query;
//        $pages = new Pagination([
//            'totalCount' => $countQuery->count(),
//            'pageSize' => 2,
////                                'route' => 'blog/',
//            'forcePageParam' => false,
//            'pageSizeParam' => false,
//        ]);
//        $articles = $query->offset($pages->offset)
//                ->limit($pages->limit)
////                
//                ->all();

        $searchModel = new \app\models\ArticlesSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        $dataProvider->pagination->pageSize = 15;
        $dataProvider = new \yii\data\ActiveDataProvider(                
                [
                      'query' => $query,
                ]
                );
//        $dataProvider->query=$query;

        return $this->render('view', [ 'model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]);


//        return $this->render('view', ['articles' => $articles, 'model' => $model]);
      } else {
        return $this->redirect(Yii::$app->request->referrer);
      }
    }
  }

}
