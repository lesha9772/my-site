<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\swiftmailer\Mailer;
use app\components\behaviors\EmailBehavior;
use app\models\User;
use app\models\SignupForm;

//use yii\base\Controller;
class SiteController extends Controller
{

  public $layout = 'digishop';

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
// ...
//        'access' => [
//            'class' => AccessControl::className(),
//            'only' => ['logout'],
//            'rules' => [
//                [
//                    'actions' => ['logout'],
//                    'allow' => true,
//                    'roles' => ['@'],
//                ],
//                [
//                    'actions' => ['login'],
//                    'allow' => true,
//                    'roles' => ['?'],
//                ],
//            ],
//        ],
//        'EmailBehavior' => [
//            'class' => EmailBehavior::className(),
//            'attributes' => [
//                Controller::EVENT_AFTER_ACTION => 'sendEmail',
//            ]
//        ],
            // ...
    ];
  }

  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
        'error' => [
            'class' => 'yii\web\ErrorAction',
        ],
        'captcha' => [
            'class' => 'yii\captcha\CaptchaAction',
            'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
        ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex($slug = null)
  {

    $categorys = \app\models\Categorys::find()->all();
    Yii::$app->params['categorys'] = $categorys;

    if (empty($slug) || $slug == 'index') {
      
    } else {
      var_dump($slug);
      die();
    }

    return $this->render('index');
  }

  /**
   * @return string
   */
  public function actionAbout()
  {
    return $this->render('about');
  }

  /**
   * @return string
   */
  public function actionSendmail()
  {


    Yii::$app->mailer->compose()
            ->setFrom('lesha9772@gmail.com')
            ->setTo('lesha97721@gmail.com')
            ->setSubject('Тема сообщения')
            ->setTextBody('Текст сообщения')
            ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
            ->send();


    return $this->render('sendmail');
  }

  /**
   * @return string
   */
  public function actionLogin()
  {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }
    return $this->render('login', [
                'model' => $model,
    ]);
  }

  /**
   * Signs user up.
   *
   * @return mixed
   */
  public function actionSignup()
  {
    $model = new SignupForm();
    if ($model->load(Yii::$app->request->post())) {
      if ($user = $model->signup()) {
        $email = \Yii::$app->mailer->compose()
                ->setTo($user->email)
                ->setFrom('lesha9772@gmail.com')
                ->setSubject('Пожалуйста, подтвердите email')
                ->setHtmlBody("
  Click this link " . \yii\helpers\Html::a('confirm', Yii::$app->urlManager->createAbsoluteUrl(
                                        ['site/confirm', 'id' => $user->id, 'key' => $user->auth_key, null]
                        ))
                )
                ->send();

        return $this->render('thank_you', ['email' => $user->email]);

//        if (Yii::$app->getUser()->login($user)) {
//          return $this->goHome();
//        }
      }
    }

    return $this->render('signup', [
                'model' => $model,
    ]);
  }

  public function actionConfirm($id, $key)
  {
    $user = User::find()->where([
                'id' => $id,
                'auth_key' => $key,
                'status' => 0,
            ])->one();

    if (!empty($user)) {
      $user->status = User::STATUS_ACTIVE;
      if ($user->save()) {
        return $this->render('thank_you_active', ['email' => $user->email]);
      } else {
        var_dump($user->errors);
      }


//      Yii::$app->getSession()->setFlash('success', 'Success!');
    } else {
      Yii::$app->getSession()->setFlash('warning', 'Failed!');
    }
    return $this->goHome();
  }

}
