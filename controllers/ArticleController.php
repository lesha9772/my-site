<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\swiftmailer\Mailer;
use app\components\behaviors\EmailBehavior;
use app\models\User;
use app\models\SignupForm;
use yii\data\Pagination;

//use yii\base\Controller;
class ArticleController extends Controller
{

  public $layout = 'digishop';

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
// ...
//        'access' => [
//            'class' => AccessControl::className(),
//            'only' => ['logout'],
//            'rules' => [
//                [
//                    'actions' => ['logout'],
//                    'allow' => true,
//                    'roles' => ['@'],
//                ],
//                [
//                    'actions' => ['login'],
//                    'allow' => true,
//                    'roles' => ['?'],
//                ],
//            ],
//        ],
//        'EmailBehavior' => [
//            'class' => EmailBehavior::className(),
//            'attributes' => [
//                Controller::EVENT_AFTER_ACTION => 'sendEmail',
//            ]
//        ],
            // ...
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex($slug = null)
  {

    if (empty($slug) || $slug == 'index') {

      // выводит список всех категорий СДЕЛАТЬ


      return $this->redirect(Yii::$app->request->referrer);
    } else {

      $model = \app\models\Articles::find()->where(['slug' => $slug])->one();

      if (!is_null($model)) {
        return $this->render('view', ['model' => $model]);
      } else {
        return $this->redirect(Yii::$app->request->referrer);
      }
    }
  }

}
