<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $title
 * @property string $slug
 * @property string $description
 * @property integer $created_at
 *
 * @property Categorys $category
 */
class Articles extends \yii\db\ActiveRecord
{

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
        [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
            ],
        ],
        'slug' => [
            'class' => 'app\components\behaviors\Slug',
            'in_attribute' => 'title',
            'out_attribute' => 'slug',
            'translit' => true
        ],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'articles';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
        [['category_id', 'created_at'], 'integer'],
        [['title'], 'required'],
        [['content'], 'safe'],
        [['description', 'content'], 'string'],
        [['title', 'slug'], 'string', 'max' => 255],
        [['slug'], 'unique'],
        [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categorys::className(), 'targetAttribute' => ['category_id' => 'id']],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
        'id' => Yii::t('admin', 'ID'),
        'category_id' => Yii::t('admin', 'Category ID'),
        'title' => Yii::t('admin', 'Title'),
        'slug' => Yii::t('admin', 'Slug'),
        'description' => Yii::t('admin', 'Description'),
        'created_at' => Yii::t('admin', 'Created At'),
        'content' => Yii::t('admin', 'Content'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategory()
  {
    return $this->hasOne(Categorys::className(), ['id' => 'category_id']);
  }
  
  

  
 
}
