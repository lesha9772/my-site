<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use app\models\Articles;

/**
 * This is the model class for table "categorys".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property integer $show
 * @property integer $created_at
 */
class Categorys extends \yii\db\ActiveRecord
{

  public $count_articles;


  /**
   * @inheritdoc
   */
  public function behaviors()
  {
    return [
        [
            'class' => TimestampBehavior::className(),
            'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
            ],
        ],
        'slug' => [
            'class' => 'app\components\behaviors\Slug',
            'in_attribute' => 'title',
            'out_attribute' => 'slug',
            'translit' => true
        ],
    ];
  }

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return 'categorys';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
        [['parent_id', 'show', 'created_at', 'count_articles'], 'integer'],
        [['title'], 'required'],
        [['title', 'slug'], 'string', 'max' => 255],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
        'id' => Yii::t('app', 'ID'),
        'parent_id' => Yii::t('app', 'Parent ID'),
        'title' => Yii::t('app', 'Title'),
        'show' => Yii::t('app', 'Show'),
        'created_at' => Yii::t('app', 'Created At'),
        'slug' => Yii::t('app', 'Slug'),
        'count_articles' => Yii::t('app', 'count_articles'),
    ];
  }

  public function getArticles()
  {
    return $this->hasMany(Articles::className(), ['category_id' => 'id']);
  }

  static public function getAllCategorys()
  {
    return static::find()
                    ->select([
                        '{{' . static::tableName() . '}}.*', // получить все атрибуты покупателя
                        'COUNT({{articles}}.id) AS count_articles' // вычислить количество заказов
                    ])
//                    ->from([static::tableName(),'articles'])
                    ->joinWith('articles')
//                    ->joinWith(['comments'=> function ($query) {
//        $query->onCondition(['comments.post_id' => '0']);
//    },])
// обеспечить построение промежуточной таблицы
//                    ->leftJoin('articles', 'articles.category_id = categorys.id')
//            ->andWhere(['comments.show' => '0'])
                    ->groupBy('{{' . static::tableName() . '}}.id') // сгруппировать результаты, чтобы заставить агрегацию работать
//                    ->orderBy([
//                        'id' => SORT_DESC])
                    ;
  }

}
