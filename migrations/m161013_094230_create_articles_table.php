<?php

use yii\db\Migration;

/**
 * Handles the creation for table `articles`.
 */
class m161013_094230_create_articles_table extends Migration
{

  /**
   * @inheritdoc
   */
  public function up()
  {
    $this->createTable('articles', [
        'id' => $this->primaryKey(),
        'category_id' => $this->integer(11),
        'title' => $this->string(255)->notNull(),
        'slug' => $this->string(255)->notNull()->unique(),
        'description' => $this->text(),
        'content' => $this->text(),
        'created_at' => $this->integer(11)->notNull(),
    ]);

    $this->addForeignKey("article_category_fk",  "articles", "category_id", "categorys", "id", 'CASCADE');
  }

  /**
   * @inheritdoc
   */
  public function down()
  {

    $this->dropForeignKey('article_category_fk', 'articles');
    $this->dropTable('articles');
  }

}
