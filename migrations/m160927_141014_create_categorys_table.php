<?php

use yii\db\Migration;

/**
 * Handles the creation for table `catalog`.
 */
class m160927_141014_create_categorys_table extends Migration
{

  /**
   * @inheritdoc
   */
  public function up()
  {
    $tableOptions = null;
    if ($this->db->driverName === 'mysql') {
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
    }
    // create table for categorys
    $this->createTable('categorys', [
        'id' => $this->primaryKey(),
        'parent_id' => $this->integer(11),
        'title' => $this->string(255)->notNull(),
        'slug' => $this->string(255)->notNull()->unique(),
        'show' => $this->integer()->defaultValue(1),
        'created_at' => $this->integer()->notNull(),
            ], $tableOptions);
  }

  /**
   * @inheritdoc
   */
  public function down()
  {
    $this->dropTable('categorys');
  }

}
