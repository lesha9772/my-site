<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=my-site',
    'username' => 'root',
    'password' => 'root',
    'charset' => 'utf8',
];
