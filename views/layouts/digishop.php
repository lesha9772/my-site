<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$categorys = \app\models\Categorys::getAllCategorys()->all();;

//        var_dump($categorys);
//        die()

//$count = (new \yii\db\Query())
//        ->from('articles')
//        ->where(['category_id' => 'Smith'])
//        ->count();
        ?>
        <?php $this->beginPage() ?>
        <html lang="<?= Yii::$app->language ?>">
            <head>
                <meta charset="<?= Yii::$app->charset ?>">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <?= Html::csrfMetaTags() ?>
                <title><?= Html::encode($this->title) ?></title>
                <?php $this->head() ?>
            </head>
            <body>
                <?php $this->beginBody() ?>
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?= \yii\helpers\Url::home() ?>"><strong>Мой сайт</strong></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#" style="cursor:default "></a></li>
                                <!--<li><a href="#">Все товары</a></li>-->
                                <!--<li><a href="#">Доставка</a></li>-->
                                <!--<li><a href="#">Контакты</a></li>-->
                                <li><a href="#" style="cursor:default "></a></li>
                                <!--                        <li class="dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Корзина <b class="caret"></b></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#"><strong>Call: </strong>+09-456-567-890</a></li>
                                                                <li><a href="#"><strong>Mail: </strong>info@yourdomain.com</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><strong>Address: </strong>
                                                                        <div>
                                                                            234, New york Street,<br />
                                                                            Just Location, USA
                                                                        </div>
                                                                    </a></li>
                                                            </ul>
                                                        </li>-->
                            </ul>
                            <form class="navbar-form navbar-right" role="search">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter Keyword Here ..." class="form-control">
                                    <select  class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>

                                    </select>
                                </div>
                                &nbsp; 
                                <button type="submit" class="btn btn-primary">Search</button>
                            </form>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
                <!--<div class="container">-->
                <div class="col-md-12">
                    <div class="row">
                        <?= $this->render('left_block', ['categorys' => $categorys]) ?>
                        <?= $content ?>
                        <?php
//                echo $this->render('right_block');
                        ?>
                    </div>
                </div>
                <!-- /.container -->

                <!--Footer -->
                <!-- /.col -->
                <div class="col-md-12 end-box ">
                    &copy; 2014 | &nbsp; All Rights Reserved | &nbsp; www.yourdomain.com | &nbsp; 24x7 support | &nbsp; Email us: info@yourdomain.com
                </div>
                <!-- /.col -->
                <!--Footer end -->
                <!--Slider JavaScript file  -->
                <script>

                </script>

                <?php $this->endBody() ?>
            </body>
        </html>
        <?php $this->endPage() ?>
