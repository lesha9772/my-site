<?php

use yii\helpers\Url;
?>

<style>
    .list-group-item>a{
        color:black;
        font-weight: 200;
    }
    .list-group-item:hover>a{
        color: blue;
    }
    .list-group-item:hover{
        cursor: pointer;
    }
</style>

<div class="col-md-3 col-lg-2">
    <div>
        <a href="#" class="list-group-item active">Уроки и статьи:
        </a>
        <ul class="list-group">


            <?php
            foreach ($categorys as $singleCategory):
              ?>
              <li class="list-group-item"><a href="<?= Url::to('/category/'.$singleCategory->slug) ?>"><?php echo $singleCategory->title;?></a>
                  <span class="label label-primary pull-right"><?php echo $singleCategory->count_articles; ?></span>
              </li>
            <?php endforeach; ?>

        </ul>
    </div>


    <!--
    
        <div>
            <a href="#" class="list-group-item active">Online-инструменты:
            </a>
            <ul class="list-group">
    
                <li class="list-group-item">Изображения
                    <span class="label label-primary pull-right">234</span>
                </li>
                <li class="list-group-item">CSS
                    <span class="label label-success pull-right">34</span>
                </li>
                <li class="list-group-item">Разное
                    <span class="label label-danger pull-right">4</span>
                </li>
                <li class="list-group-item">Подобрать хостинг
                    <span class="label label-info pull-right">434</span>
                </li>
            </ul>
        </div>-->
    <!-- /.div -->
</div>