  <?php
//var_dump($model);
//echo '<br /><br /><br />';
//var_dump($articles);
?>

<?php

use app\components\widgets\AlexMetaTagsWidget;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\grid\GridView;

/* @var $this yii\web\View */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('admin', 'Categorys'), 'url' => ['/category']];
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- /.row -->


<!-- /.col -->
<div class="col-md-6 col-lg-8">
    <?php
//    Pjax::begin();
    ?>
    <?=
    Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ])
    ?>

    <div class="row">
        <h1><?php echo $model->title; ?></h1>
    </div>

    <div class="row">

        <?php
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [ 'attribute' => 'title',
                    'format'=>'raw',
                    'value'=>function($model){
          
                    return '<a href="'.Url::to('/article/'.$model->slug).'">'.$model->title.'</a>';
                    },
//                'label' => 'Короткое описание',
                    'headerOptions' => ['style' => 'width:500'],
                    'contentOptions' => ['style' => 'max-width: 500px;overflow: auto;']
                ],
                [ 'attribute' => 'description',
//                'label' => 'Короткое описание',
                    'headerOptions' => ['width' => '200'],
                    'contentOptions' => ['style' => 'max-width: 200px;overflow: auto;']
                ],
                [ 'attribute' => 'created_at',
//                'label' => 'Короткое описание',
                    'filter' => FALSE,
                    'headerOptions' => ['width' => '200'],
                    'contentOptions' => ['style' => 'max-width: 200px; width:200px; overflow: auto;'],
                    'filterOptions' => ['style' => 'max-width: 200px; width:200px; overflow: auto;'],
                    'format' => ['date', 'HH:mm dd-MM-Y'],
                ],
            ],
        ]);
        ?>


        <!--        <div class="col-md-4 text-center col-sm-6 col-xs-6">
                    <div class="thumbnail product-box">
                        <img src="assets_new/img/dummyimg.png" alt="" />
                        <div class="caption">
                            <h3><a href="#">Samsung Galaxy </a></h3>
                            <p>Price : <strong>$ 3,45,900</strong>  </p>
                            <p><a href="#">Ptional dismiss button </a></p>
                            <p>Ptional dismiss button in tional dismiss button in   </p>
                            <p><a href="#" class="btn btn-success" role="button">Add To Cart</a> <a href="#" class="btn btn-primary" role="button">See Details</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center col-sm-6 col-xs-6">
                    <div class="thumbnail product-box">
                        <img src="assets_new/img/dummyimg.png" alt="" />
                        <div class="caption">
                            <h3><a href="#">Samsung Galaxy </a></h3>
                            <p>Price : <strong>$ 3,45,900</strong>  </p>
                            <p><a href="#">Ptional dismiss button </a></p>
                            <p>Ptional dismiss button in tional dismiss button in   </p>
                            <p><a href="#" class="btn btn-success" role="button">Add To Cart</a> <a href="#" class="btn btn-primary" role="button">See Details</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 text-center col-sm-6 col-xs-6">
                    <div class="thumbnail product-box">
                        <img src="assets_new/img/dummyimg.png" alt="" />
                        <div class="caption">
                            <h3><a href="#">Samsung Galaxy </a></h3>
                            <p>Price : <strong>$ 3,45,900</strong>  </p>
                            <p><a href="#">Ptional dismiss button </a></p>
                            <p>Ptional dismiss button in tional dismiss button in   </p>
                            <p><a href="#" class="btn btn-success" role="button">Add To Cart</a> <a href="#" class="btn btn-primary" role="button">See Details</a></p>
                        </div>
                    </div>
                </div>-->
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">

        <?php
        // display pagination
//        echo LinkPager::widget([
//            'pagination' => $pages,
//            'activePageCssClass' => 'active',
//            'options' => ['class' => 'pagination alg-right-pad'],
//        ]);
        ?>
    </div>


    <?php
//    Pjax::end();
    ?>
    <!-- /.row -->
    <!-- /.div -->
    <!-- /.row -->
    <!-- /.row -->
    <!-- /.row -->
</div>

<!-- /.col -->
<!-- /.row -->
