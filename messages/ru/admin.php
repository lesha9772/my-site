<?php

return [
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'Categorys' => 'Категории',
    'Create Categorys' => 'Добавить категорию',
    'Update service: ' => 'Обновить услугу: ',
    'Title' => 'Название',
    'Description' => 'Короткое описание',
    'Category' => 'Категория',
    'Created At' => 'Создан',
    'Create Articles' => 'Добавить статтю',
    'Articles' => 'Статти',
];
