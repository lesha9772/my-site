<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{

  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
//        'assets_new/css/bootstrap.css',
      'assets_new/css/font-awesome.min.css',
      'http://fonts.googleapis.com/css?family=Open+Sans',
      'assets_new/ItemSlider/css/main-style.css',
      'assets_new/css/style.css'
  ];
  public $js = [
      'assets_new/ItemSlider/js/modernizr.custom.63321.js',
      'assets_new/ItemSlider/js/jquery.catslider.js'
  ];
  public $depends = [
      'yii\web\YiiAsset',
      'yii\bootstrap\BootstrapAsset',
      'yii\bootstrap\BootstrapPluginAsset',
      'uran1980\yii\assets\codePrettify\CodePrettifyAsset',
  ];

}
